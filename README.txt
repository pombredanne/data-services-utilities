This is a collection of helpful utilities used by the team.

commands/
    command-line utilities, such as doing certain wrangling to a db

scripts/
    snippets we might install on a server for example to wrap another program
    with log output
